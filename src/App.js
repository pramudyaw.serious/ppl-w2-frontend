import logo from "./logo.svg";
import "./App.css";
import { useEffect, useState } from "react";
import { getHelloObj } from "./api";

function App() {
  const [persons, setPersons] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const response = await getHelloObj("Pram");
      console.log(response);
      setPersons(response);
    }

    fetchData();
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        {persons.map((person, index) => (
          <p>{index+1}. {person.description}</p>
        ))}
      </header>
    </div>
  );
}

export default App;
