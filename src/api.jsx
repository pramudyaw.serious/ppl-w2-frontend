import axios from 'axios';

const BACKEND_URL_LOCAL = 'http://localhost:8000';

export const getHelloObj = async (name) => {
  const result = await axios
    .get(`${BACKEND_URL_LOCAL}/${name}`)
    .then((response) => {
      return response.data;
    });
  return result;
};

